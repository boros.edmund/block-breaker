﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] float screenWidthInUnits = 16f;
    [SerializeField] float paddlePivutDiff = 0.5f;
    [SerializeField] float minPaddleXInUnits = 1f;
    [SerializeField] float maxPaddleXInUnits = 15f;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 paddlePos = new Vector2(transform.position.x, transform.position.y);
        paddlePos.x = Mathf.Clamp(GetXPos(), minPaddleXInUnits, maxPaddleXInUnits);
        transform.position = paddlePos;
        
    }
    private float GetXPos()
    {
        float paddleNextXInUnits = Input.mousePosition.x / Screen.width * screenWidthInUnits;
        if (FindObjectOfType<GameSession>().IsAutoPlayEnabled())
        {
            return FindObjectOfType<Ball>().transform.position.x;
        } 
        return paddleNextXInUnits;
    }
}
